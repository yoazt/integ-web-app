import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";

import FormInput from "modules/common/forms/FormInput";
import FormInputPassword from "modules/common/forms/FormInputPassword";
import { useAuth } from "modules/auth/hooks";

function RegistrationContainer() {
  const history = useHistory();
  const { register, handleSubmit } = useForm({});
  const { register: registerAccount } = useAuth();

  const handleRegister = (data) => {
    const { email, password } = data;
    registerAccount(email, password);
  };

  const handleRedirectToLogin = () => {
    history.push("/");
  };

  return (
    <form
      className="h-full flex flex-col justify-center gap-3 relative"
      onSubmit={handleSubmit(handleRegister)}
    >
      <button
        type="button"
        className="absolute top-0 left-0 btn text-gray-600 font-semibold border-2 border-gray-600"
        onClick={handleRedirectToLogin}
      >
        <i className="fa fa-angle-left mr-3" />
        Back to Login
      </button>
      <h2 className="mb-10 text-3xl text-center text-gray-800 font-semibold">
        Register to iCam Portal
      </h2>
      <FormInput label="Email" name="email" validation={register} />
      <FormInputPassword
        label="Password"
        name="password"
        validation={register}
      />
      <button className="btn primary lg font-semibold" type="submit">
        Create My Account
      </button>
    </form>
  );
}

export default RegistrationContainer;
