export const authFetch = (url: string) => async (): Promise<any> => {
  const token = localStorage.getItem("_token");
  const respose = await fetch(url, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return respose.json();
};
