import React from "react";
import { get } from "lodash";

interface Props {
  format: Array<any>;
  data: Array<any>;
  //   isLoading?: boolean;
  //   emptyLabel?: string;
  hideHeader?: boolean;
}

const GenericTable: React.FC<Props> = ({
  format,
  data,
  //   isLoading,
  //   emptyLabel,
  hideHeader,
}) => {
  const renderContent = React.useCallback((row, col, rowIndex, colIndex) => {
    if (typeof col.key === "function") return col.key(row, rowIndex, colIndex);
    return get(row, col.key) || "";
  }, []);
  return (
    <div className="flex-grow relative mx-1 overflow-y-scroll">
      <table className="generic-table table-auto w-full">
        {!hideHeader && (
          <thead className="bg-transparent ">
            <tr>
              {format.map((col, i) => {
                const key = i;
                return (
                  <th
                    className={
                      col.className ||
                      "bg-gray-200 text-gray-700 px-4 py-3 text-xs text-left font-normal"
                    }
                    key={key}
                    style={{ width: col.width || "auto" }}
                  >
                    {col.label}
                  </th>
                );
              })}
            </tr>
          </thead>
        )}
        <tbody className="overflow-y-scroll">
          {data.length < 1
            ? [0, 1, 2].map((row) => (
                <tr key={row} className="animate-pulse">
                  {format.map((cell) => (
                    <td key={`${row}-${cell.label}`} className="h-8">
                      <div className="w-full h-5 rounded-md bg-gray-300" />
                    </td>
                  ))}
                </tr>
              ))
            : data.map((row, i) => {
                const rowKey = i;
                return (
                  <tr
                    key={rowKey}
                    className="bg-white hover:bg-gray-100 focus:bg-gray-200"
                  >
                    {format.map((col, j) => {
                      const colKey = j;
                      return (
                        <td
                          className={
                            col.className ||
                            "font-light px-4 py-3 text-sm border-b "
                          }
                          key={`${rowKey}-${colKey}`}
                          style={{ width: col.width || "auto" }}
                        >
                          {renderContent(row, col, i, j)}
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
        </tbody>
      </table>
    </div>
  );
};

export default React.memo(GenericTable);
