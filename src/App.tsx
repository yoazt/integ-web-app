import { QueryClient, QueryClientProvider } from "react-query";

import { useAuth } from "modules/auth/hooks";
import Public from "Public";
import Private from "Private";

const queryClient = new QueryClient();

function App() {
  const { token } = useAuth();

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <div className="bg-customGray w-full h-full fixed top-0 left-0 overflow-auto">
          {!token ? <Public /> : <Private />}
        </div>
      </QueryClientProvider>
    </>
  );
}

export default App;
