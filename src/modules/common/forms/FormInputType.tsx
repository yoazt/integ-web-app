import React from "react";

interface formInputTypeProps {
  name: string;
  age: number;
}

const FormInputType: React.FC<formInputTypeProps> = ({ age, name }) => {
  return (
    <div>
      <h2>{name}</h2>
      <p>{age}</p>
    </div>
  );
};

export default FormInputType;
