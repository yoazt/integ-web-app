import { useEffect, useState } from "react";
import firebase from "lib/firebase";

export const useAuth = () => {
  const [token, setToken] = useState(localStorage.getItem("_token") || "");

  useEffect(() => {
    firebase.auth().onAuthStateChanged(async function (result) {
      const fbToken = await result?.getIdToken();
      setToken(fbToken || "");
      localStorage.setItem("_token", fbToken || "");
    });
  }, []);

  function register(email: string, password: string) {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((user) => user);
  }

  function signin(email: string, password: string) {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((userCredential) => {
        console.log("info: ", userCredential.user?.metadata);
      });
  }

  function signout() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        setToken("");
        localStorage.removeItem("_token");
      });
  }

  return {
    register,
    signin,
    signout,
    token,
  };
};
