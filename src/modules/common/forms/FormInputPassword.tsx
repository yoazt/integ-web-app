import React, { ChangeEvent } from "react";
import { useState } from "react";

interface Props {
  name: string;
  label: string;
  validation?: () => void;
  required?: boolean;
  error?: Error;
  leftIcon?: Element;
  rightIcon?: Element;
}

const FormInputPassword: React.FC<Props> = ({
  name,
  label,
  error,
  validation,
  required,
  leftIcon,
  rightIcon,
  ...rest
}) => {
  const [labelIsOnTop, setLabelIsOnTop] = useState(false);
  const [inputType, setInputType] = useState("password");

  const handleTogglePassword = () => {
    if (inputType === "password") setInputType("text");
    else setInputType("password");
  };

  const handleFocus = () => {
    setLabelIsOnTop(true);
  };

  const handleBlur = (e: ChangeEvent<any>) => {
    const { value } = e.target;
    if (!value) {
      setLabelIsOnTop(false);
    }
  };

  return (
    <div className="flex items-center relative w-full">
      {label && (
        <label
          htmlFor={name}
          className={`absolute ml-3 top-1/2 text-gray-600 font-medium 
                     transition-all duration-75 ease-in pointer-events-none
                     `}
          style={{
            transform: !labelIsOnTop
              ? "translateY(-50%) scale(1)"
              : "translate(-15%, -120%) scale(.7)",
          }}
        >
          {label}
          {required && <span className="text-red-600 mr-1">*</span>}
        </label>
      )}
      <span className="absolute left-0 ml-4 ">{leftIcon}</span>
      <input
        type={inputType}
        ref={validation}
        name={name}
        className={`
            bg-white text-gray-900 w-full px-3 pb-2 pt-5 h-14 rounded-lg mb-0 bg-transparent font-medium outline-none border
            ${error ? "border-red-500" : ""} 
            ${rightIcon ? "pr-10" : ""}  
            ${leftIcon ? "pl-10" : "'"} 
           `}
        onFocus={handleFocus}
        onBlur={handleBlur}
        {...rest}
      />
      <button
        type="button"
        className="absolute right-0 mr-4 text-sm cursur text-gray-600 hover:text-primary-600"
        aria-hidden="true"
        onClick={handleTogglePassword}
        style={{
          top: "50%",
          transform: "translateY(-50%)",
        }}
      >
        {inputType === "password" ? "Show" : "Hide"}
      </button>
      <span className="absolute px-4 right-0">{rightIcon}</span>
      {error && (
        <small className="text-xs text-red-500">
          <i className="fa fa-exclamation-circle" /> {error.message}
        </small>
      )}
    </div>
  );
};

export default FormInputPassword;
