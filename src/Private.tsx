// import { useQuery } from "react-query";
import { Switch, Route } from "react-router-dom";

import Sidebar from "modules/partial/components/Sidebar";
// import Header from "modules/partial/components/Header";
import DashboardContainer from "modules/dashboard/containers/DashboardContainer";
import ProjectsContainer from "modules/projects/containers/ProjectsContainer";

function Private() {
  return (
    <div className="flex h-screen w-full">
      <Sidebar />
      <main className="flex-grow h-full flex flex-col">
        {/* <Header /> */}
        <div className="flex-grow">
          <Switch>
            <Route exact path="/" component={DashboardContainer} />
            <Route path="/projects" component={ProjectsContainer} />
          </Switch>
        </div>
      </main>
      {/* <footer className="fixed left-0 bottom-0 w-full h-11 bg-white border-t-2 flex justify-center items-center">
        <p className="text-gray-800">iCAM @{new Date().getFullYear()}</p>
      </footer> */}
    </div>
  );
}

export default Private;
