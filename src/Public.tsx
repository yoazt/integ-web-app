import { Switch, Route, Redirect } from "react-router-dom";

import Login from "modules/auth/containers/Login";
import RegistrationContainer from "modules/registration/containers/RegistrationContainer";

function Public() {
  return (
    <div className="h-screen w-full flex">
      <aside className="w-full md:w-96 h-full bg-white p-4 flex justify-center">
        <div className="w-96">
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/register" component={RegistrationContainer} />
            <Route render={() => <Redirect to="/" />} />
          </Switch>
        </div>
      </aside>
      <div className="hidden md:block flex-grow bg-gradient-to-tr from-primary-400 to-primary-800 h-full"></div>
    </div>
  );
}

export default Public;
