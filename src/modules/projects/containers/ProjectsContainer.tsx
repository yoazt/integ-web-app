import { useQuery } from "react-query";

import GenericTable from "modules/common/components/GenericTable";
import { authFetch } from "modules/common/helpers";

const ProjectsContainer = () => {
  const { data: projectsData } = useQuery(
    "projectList",
    authFetch("https://falcon-ws.yakadapp.com/v1/projects")
  );

  const projectTableFormat = [
    {
      label: "NAME",
      key: "name",
    },
    {
      label: "UPDATED AT",
      key: "updatedAt",
    },
    {
      label: "STATUS",
      key: "status",
    },
  ];

  return (
    <div className="p-4">
      <GenericTable
        data={projectsData?.data?.items || []}
        format={projectTableFormat}
      />
    </div>
  );
};

export default ProjectsContainer;
