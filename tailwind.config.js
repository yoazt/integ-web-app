module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          800: "#3FCB88",
          400: "#13EB83",
        },
        secondary: {
          800: "#f0c08a",
          400: "#FCF1E4",
        },
        customGray: "#F2F2F2",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
