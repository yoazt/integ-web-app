import { useAuth } from "modules/auth/hooks";

const Header = () => {
  const { signout } = useAuth();

  const handleSignout = () => {
    signout();
  };
  return (
    <header className="h-16 w-full flex items-center bg-gradient-to-r from-primary-800 to-primary-400">
      <div className="ml-auto mr-4 text-white text-xl">
        <button>
          <i className="fa fa-bell" />
        </button>
        <button onClick={handleSignout}>
          <i className="ml-4 fa fa-sign-out-alt" />
        </button>
      </div>
    </header>
  );
};

export default Header;
