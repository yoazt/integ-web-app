import firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage";

firebase.initializeApp({
  apiKey: process.env.REACT_APP_FIREBASE_APIKEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAIN,
  //   databaseURL: "<your-database-url>",
  projectId: process.env.REACT_APP_FIREBASE_PROJECTID,
  storageBucket: "gs://falcon-uae-prod.appspot.com",
  //   messagingSenderId: "<your-sender-id>",
});

export const storage = firebase.app().storage();
export default firebase;
