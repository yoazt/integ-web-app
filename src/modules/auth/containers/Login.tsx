import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";

import FormInput from "modules/common/forms/FormInput";
import FormInputPassword from "modules/common/forms/FormInputPassword";
import { useAuth } from "modules/auth/hooks";

function Login() {
  const history = useHistory();
  const { register, handleSubmit } = useForm({});
  const { signin } = useAuth();

  const handleLogin = (data: any) => {
    const { email, password } = data;
    signin(email, password);
  };

  const handleRedirectToRegister = () => {
    history.push("/register");
  };

  return (
    <form
      className="h-full flex flex-col justify-center"
      onSubmit={handleSubmit(handleLogin)}
    >
      <h2 className="mt-3 mb-10 text-3xl text-center text-gray-800 font-semibold">
        Sign in to iCam Partners
      </h2>
      <div className="mt-3">
        <FormInput label="Email" name="email" validation={register} />
      </div>
      <div className="mt-3">
        <FormInputPassword
          label="Password"
          name="password"
          validation={register}
        />
      </div>
      <p className="text-xs text-gray-800 self-end my-4">
        Don't have an account?{" "}
        <button
          className="text-primary-800"
          type="button"
          onClick={handleRedirectToRegister}
        >
          Create account
        </button>
      </p>
      <button className="btn primary lg font-semibold" type="submit">
        Login
      </button>
    </form>
  );
}

export default Login;
