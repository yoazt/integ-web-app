import { useState, useEffect } from "react";
import firebase from "lib/firebase";

export const useAccountAttributes = () => {
  const [accountAttributes, setAccountAttributes] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    firebase.auth().onAuthStateChanged((res) => {
      setAccountAttributes((prev) => ({ ...prev, meta: res?.metadata }));
      setIsLoading(false);
    });
  }, []);

  return [accountAttributes, isLoading];
};
