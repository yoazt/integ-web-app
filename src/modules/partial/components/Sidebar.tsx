import { useQuery } from "react-query";
import { NavLink } from "react-router-dom";

import AvatarSrc from "assets/images/avatar.png";
import { useAccountAttributes } from "modules/account/hooks";
import { useAuth } from "modules/auth/hooks";
import { authFetch } from "modules/common/helpers";

const Sidebar = () => {
  const { signout } = useAuth();
  const [{ meta }, isLoading]: any = useAccountAttributes();

  const { data: profileData } = useQuery(
    "me",
    authFetch("https://falcon-ws.yakadapp.com/v1/user")
  );

  const handleSignout = () => {
    signout();
  };

  return (
    <aside className="sidebar">
      <div className="account">
        <div className="flex">
          <img
            className="avatar w-14 h-14 rounded-full"
            alt=""
            src={AvatarSrc}
          />
          <div className="ml-4 flex flex-col">
            <p className="text-lg text-gray-800">Hello</p>
            <h4 className={`text-primary-800 text-xl font-bold`}>
              {profileData?.data?.username || (
                <div className="rounded w-32 h-6 bg-gray-300 animate-pulse" />
              )}
            </h4>
          </div>
          <div className="ml-auto">
            <button onClick={handleSignout}>
              <i className="ml-4 fa fa-sign-out-alt" />
            </button>
          </div>
        </div>
        {isLoading ? (
          <div className="mt-6 animate-pulse">
            <div className="w-full h-2 rounded-sm bg-gray-300" />
            <div className="mt-2 w-1/4 h-2 rounded-sm bg-gray-300" />
          </div>
        ) : (
          <p className="mt-4 text-xs text-gray-700">
            Your last login was on {`${meta?.lastSignInTime}`}
          </p>
        )}
      </div>
      <nav className="nav-items">
        <NavLink className="nav-item" exact to="/">
          <span className="icon">
            <i className="fa fa-home" />
          </span>
          <span>Dashboard</span>
        </NavLink>

        <NavLink className="nav-item" to="/projects">
          <span className="icon">
            <i className="far fa-folder" />
          </span>
          <span>Projects</span>
        </NavLink>

        <NavLink className="nav-item" exact to="/history">
          <span className="icon">
            <i className="fa fa-history" />
          </span>
          <span>History</span>
        </NavLink>

        <NavLink className="nav-item" exact to="/settings">
          <span className="icon">
            <i className="fa fa-sliders-h" />
          </span>
          <span>Settings</span>
        </NavLink>
      </nav>
    </aside>
  );
};

export default Sidebar;
